<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>Candies</h2>
                <table border="3">
                    <tr bgcolor="#9acd32">
                        <th>Name</th>
                        <th>Energy</th>
                        <th>Type</th>
                        <th>Water</th>
                        <th>Sugar</th>
                        <th>Fructose</th>
                        <th>vanillin</th>
                        <th>proteins</th>
                        <th>fats</th>
                        <th>carbohydrates</th>
                        <th>production</th>
                    </tr>
                    <xsl:for-each select="candies/candy">
                        <tr bgcolor="#D59069">
                            <td>
                                <xsl:value-of select="name"/>
                            </td>
                            <td>
                                <xsl:value-of select="energy"/>
                            </td>
                            <td>
                                <xsl:value-of select="type"/>
                            </td>
                            <td>
                                <xsl:value-of select="ingredients/water"/>
                            </td>
                            <td>
                                <xsl:value-of select="ingredients/sugar"/>
                            </td>
                            <td>
                                <xsl:value-of select="ingredients/fructose"/>
                            </td>
                            <td>
                                <xsl:value-of select="ingredients/vanillin"/>
                            </td>
                            <td>
                                <xsl:value-of select="value/proteins"/>
                            </td>
                            <td>
                                <xsl:value-of select="value/fats"/>
                            </td>
                            <td>
                                <xsl:value-of select="value/carbohydrates"/>
                            </td>
                            <td>
                                <xsl:value-of select="production"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>