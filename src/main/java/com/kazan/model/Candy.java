package com.kazan.model;

public class Candy {

    private String name;
    private int energy;
    private Type type;
    private Ingredient ingredient;
    private Value value;
    private String production;

    public Candy() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    @Override
    public String toString() {
        return "Candy{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", ingredient=" + ingredient +
                ", value=" + value +
                ", production='" + production + '\'' +
                '}';
    }
}
