package com.kazan.model;

public enum Type {

    CHOCOLATE,
    CARAMELS,
    IRIS,
    LOLLIPOP

}
