package com.kazan.view;

import com.kazan.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    Scanner scanner = new Scanner(System.in);
    Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("1", "1 - Task 1 - Parse by DOM");
        menu.put("2", "2 - Task 2 - Parse by SAX");
        menu.put("3", "3 - Task 3 - Parse by StAX");
        menu.put("4", "4 - Task 4 - Transform into HTML and back");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", this::doParseByDOM);
        menuMethods.put("2", this::doParseBySAX);
        menuMethods.put("3", this::doParseByStAX);
        menuMethods.put("4", this::doTransform);
    }

    private void doParseByDOM() {
        controller.parseByDomParser();
    }

    private void doParseBySAX() {
        controller.parseBySAXParser();
    }

    private void doParseByStAX() {
        controller.parseByStAXParser();
    }

    private void doTransform() {
        controller.transformIntoHTMLAndBack();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }

}
