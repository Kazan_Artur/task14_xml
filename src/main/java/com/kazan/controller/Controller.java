package com.kazan.controller;

import com.kazan.parser.XMLTransformer;
import com.kazan.parser.dom.DomRunner;
import com.kazan.parser.sax.SaxRunner;
import com.kazan.parser.stax.StAXRunner;


public class Controller {

    DomRunner domRunner;
    SaxRunner saxRunner;
    StAXRunner stAXRunner;
    XMLTransformer xmlTransformer;

    public Controller() {
        domRunner = new DomRunner();
        saxRunner = new SaxRunner();
        stAXRunner = new StAXRunner();
        xmlTransformer = new XMLTransformer();
    }

    public void parseByDomParser() {
        domRunner.run();
    }

    public void parseBySAXParser() {
        saxRunner.run();
    }

    public void parseByStAXParser() {
        stAXRunner.run();
    }

    public void transformIntoHTMLAndBack() {
        xmlTransformer.transformInfoHTMLAndBack();
    }
}
