package com.kazan.parser.sax;

import com.kazan.model.Candy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

public class SaxRunner {

    private static Logger log = LogManager.getLogger(SaxRunner.class);
    final File xmlFile = new File("src/main/resources/candies.xml");
    final File xsdFile = new File("src/main/resources/candies.xsd");

    public void run() {
        SaxParser saxParser = new SaxParser();
        if (saxParser != null) {
            log.info("Start parsing by SAX parser");
        }
        List<Candy> candies = saxParser.parse(xmlFile, xsdFile);
        candies.forEach(System.out::println);
        log.info("Successfully parsed");
    }

}
