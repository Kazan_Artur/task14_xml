package com.kazan.parser.sax;

import com.kazan.model.Candy;
import com.kazan.parser.dom.XmlValidator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SaxParser {

    public List<Candy> parse(File xml, File xsd) {
        List<Candy> candies = new ArrayList<>();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setSchema(XmlValidator.createSchema(xsd));
            SAXParser saxParser = factory.newSAXParser();
            factory.setValidating(true);
            CandyHandler saxHandler = new CandyHandler();
            saxParser.parse(xml, saxHandler);
            candies = saxHandler.getCandiesList();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
        }
        return candies;
    }
}
