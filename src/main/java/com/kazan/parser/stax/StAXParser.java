package com.kazan.parser.stax;

import com.kazan.model.*;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXParser {

    public static List<Candy> parse(File xml) {
        List<Candy> flowerList = new ArrayList<>();
        Candy candy = null;
        Ingredient ingredient = null;
        Value value = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "candy":
                            candy = new Candy();
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "energy":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setType(Type.valueOf(xmlEvent.asCharacters().getData().toUpperCase()));
                            break;
                        case "ingredients":
                            xmlEvent = xmlEventReader.nextEvent();
                            ingredient = new Ingredient();
                            break;
                        case "water":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredient != null;
                            ingredient.setWater(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "sugar":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredient != null;
                            ingredient.setSugar(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "fructose":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredient != null;
                            ingredient.setFructose(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "vanillin":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredient != null;
                            ingredient.setVanillin(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            value = new Value();
                            break;
                        case "proteins":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setProteins(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "fats":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setFats(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "carbohydrates":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setCarbohydrates(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "production":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setProduction(xmlEvent.asCharacters().getData());
                            candy.setValue(value);
                            candy.setIngredient(ingredient);
                            break;
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("candy")) {
                        flowerList.add(candy);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return flowerList;
    }

}
