package com.kazan.parser.stax;

import com.kazan.model.Candy;
import com.kazan.parser.sax.SaxRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

public class StAXRunner {

    private static Logger log = LogManager.getLogger(SaxRunner.class);
    final File xmlFile = new File("src/main/resources/candies.xml");

    public void run() {
        StAXParser saxParser = new StAXParser();
        if (saxParser != null) {
            log.info("Start parsing by StAX parser");
        }
        List<Candy> candies = saxParser.parse(xmlFile);
        candies.forEach(System.out::println);
        log.info("Successfully parsed");
    }

}
