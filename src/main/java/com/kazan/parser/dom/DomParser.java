package com.kazan.parser.dom;

import com.kazan.model.*;
import org.w3c.dom.*;

import java.util.ArrayList;
import java.util.List;

public class DomParser {

    public List<Candy> parse(Document document) {
        document.getDocumentElement().normalize();
        List<Candy> candies = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName("candy");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Candy candy = new Candy();
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                candy.setName(element.getElementsByTagName("name")
                        .item(0).getTextContent());
                candy.setEnergy(Integer.parseInt(element.getElementsByTagName("energy")
                        .item(0).getTextContent()));
                candy.setType(Type.valueOf(element.getElementsByTagName("type")
                        .item(0).getTextContent().toUpperCase()));
                candy.setProduction(element.getElementsByTagName("production")
                        .item(0).getTextContent());
                candy.setIngredient(getIngredients(element.getElementsByTagName("ingredients")));
                candy.setValue(getValues(element.getElementsByTagName("value")));
                candies.add(candy);
            }
        }
        return candies;
    }

    private Ingredient getIngredients(NodeList nodes) {
        Ingredient ingredient = new Ingredient();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            Node waterNode = element.getElementsByTagName("water").item(0);
            int water = Integer.parseInt(waterNode.getTextContent());
            ingredient.setWater(water);
            Node sugarNode = element.getElementsByTagName("sugar").item(0);
            int sugar = Integer.parseInt(sugarNode.getTextContent());
            ingredient.setSugar(sugar);
            ingredient.setVanillin(Integer.parseInt(element.getElementsByTagName("vanillin").item(0).getTextContent()));
            ingredient.setFructose(Integer.parseInt(element.getElementsByTagName("fructose").item(0).getTextContent()));
        }
        return ingredient;
    }

    private Value getValues(NodeList nodes) {
        Value value = new Value();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            value.setCarbohydrates(Integer.parseInt(element.getElementsByTagName("carbohydrates").item(0).getTextContent()));
            value.setProteins(Integer.parseInt(element.getElementsByTagName("proteins").item(0).getTextContent()));
            value.setFats(Integer.parseInt(element.getElementsByTagName("fats").item(0).getTextContent()));
        }
        return value;
    }
}
