package com.kazan.parser.dom;

import com.kazan.model.Candy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class DomRunner {
    private static Logger log = LogManager.getLogger(DomRunner.class);

    final File xmlFile = new File("src/main/resources/candies.xml");
    final File xsdFile = new File("src/main/resources/candies.xsd");

    public void run() {
        Document document = buildDoc(xmlFile);
        if (document != null) {
            log.info("Document successfully built");
        } else log.info("Problem with building Document");
        DomParser domParser = new DomParser();
        if (domParser != null) {
            log.info("Start parsing by DOM parser");
        }
        if (XmlValidator.validate(document, xsdFile)) {
            List<Candy> list = domParser.parse(document);
            list.forEach(System.out::println);
        } else {
            log.info("XML document failed validation.");
        }
        log.info("Successfully parsed");
    }

    public Document buildDoc(File xml) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder dBuilder;
        Document document = null;
        try {
            dBuilder = factory.newDocumentBuilder();
            document = dBuilder.parse(xml);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        document.getDocumentElement().normalize();
        return document;
    }
}
